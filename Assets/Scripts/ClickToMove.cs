﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickToMove : MonoBehaviour
{
    public LayerMask layerMask;

    private HeroBrain brain;

    private void Awake()
    {
        brain = GetComponent<HeroBrain>();
    }
    
    void Update()
    {
        if (Input.GetMouseButtonDown((int)MouseButton.Left))
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, layerMask))
            {
                brain.TrySwichState(new MoveToPosition(brain, hit.point));
            }
        }
    }
}
