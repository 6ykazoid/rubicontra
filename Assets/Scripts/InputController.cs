﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{
    [SerializeField]
    private LayerMask layerMask;

    public event Action<GameObject, Vector3> OnLeftClick = delegate { };
    public event Action<GameObject, Vector3> OnRightClick = delegate { };
    public event Action<IAttackable, bool> OnHoverOverAttackable = delegate { };

    private bool isOnAttackable = false;

    void Update()
    {
        HandleMouseInput();
    }

    private void HandleMouseInput()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, layerMask))
        {
            IAttackable target = hit.collider.GetComponent<IAttackable>();

            if (!isOnAttackable && target != null)
            {
                isOnAttackable = true;
                OnHoverOverAttackable(target, true);
            }
            else if (isOnAttackable && target == null)
            {
                isOnAttackable = false;
                OnHoverOverAttackable(target, false);
            }

            if (Input.GetMouseButtonDown((int)MouseButton.Left))
            {
                OnLeftClick(hit.collider.gameObject, hit.point);
            }

            if (Input.GetMouseButtonDown((int)MouseButton.Right))
            {
                OnRightClick(hit.collider.gameObject, hit.point);
            }
        }
    }
}
