﻿using UnityEngine;

public class Main : MonoBehaviour
{
    public static Main Instance { get; private set; }

    public InputController InputController { get; private set; }
    public PartyController PartyController { get; private set; }
    
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        if (Instance != this)
        {
            Destroy(this);
        }

        CreateControllers();
    }

    private void CreateControllers()
    {
        GameObject gameObject = new GameObject("[Controllers]");
        InputController = gameObject.AddComponent<InputController>();
        PartyController = gameObject.AddComponent<PartyController>();
    }
}
