﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartyController : MonoBehaviour
{
    private Dictionary<HeroBrain, bool> partyDict;
    private List<HeroBrain> selectedHeroes;

    private void Start()
    {
        selectedHeroes = new List<HeroBrain>();
        var partyArr = FindObjectsOfType<HeroBrain>();
        partyDict = new Dictionary<HeroBrain, bool>(partyArr.Length);
        foreach (var hero in partyArr)
        {
            partyDict.Add(hero, true);
        }

    }

    private void MoveToPosition(Vector3 destination)
    {
        if (selectedHeroes.Count == 0)
        {
            return;
        }

        foreach (var hero in selectedHeroes)
        {
            hero.TrySwichState(new MoveToPosition(hero, destination));
        }
    }
}
