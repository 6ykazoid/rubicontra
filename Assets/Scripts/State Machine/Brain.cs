﻿using UnityEngine;
using UnityEngine.AI;

[SelectionBase]
[RequireComponent(typeof(NavMeshAgent))]
public class Brain : MonoBehaviour, IAttackable
{
    private CharacterState currentState;

    public NavMeshAgent Navmeshagent { get; private set; }

    public virtual CharacterState DefaultState()
    {
        return new Idle(this);
    }

    private void Awake()
    {
        Navmeshagent = GetComponent<NavMeshAgent>();
        SwitchState(DefaultState());
    }

    private void Brain_OnDeath()
    {
        Navmeshagent.enabled = false;
        this.enabled = false;
    }

    private void Update()
    {
        if (currentState != null)
        {
            currentState.Tick();

            if (currentState.NextState != null)
            {
                SwitchState(currentState.NextState);
            }
        }
    }

    protected void SwitchState(CharacterState newState)
    {
        if (currentState != null)
        {
            currentState.ExitState();
        }

        currentState = newState;
        currentState.EnterState();
    }

    public override string ToString()
    {
        if (currentState != null) return currentState.ToString();

        return "NO STATE";
    }

    public void TakeDamage(int amount)
    {
    }
}