﻿using UnityEditor;
using UnityEngine;
using UnityEngine.AI;

[ExecuteInEditMode]
public class BrainDebugger : MonoBehaviour
{
    [SerializeField]
    private float verticalTextOffset;
    [SerializeField]
    private int fontSize = 10;
    [SerializeField]
    private Color fontColor = Color.black;
    [SerializeField]
    private float pathOfset = 0.5f;
    [SerializeField]
    private Color pathColor = Color.red;

    private Brain brain;
    private GUIStyle style;

    private void Awake()
    {
        brain = GetComponent<Brain>();

        style = new GUIStyle()
        {
            normal = new GUIStyleState() { textColor = fontColor },
            fontSize = this.fontSize,
            alignment = TextAnchor.UpperLeft
        };
    }

    private void OnDrawGizmos()
    {
        Handles.Label(transform.position + Vector3.up * verticalTextOffset, brain.ToString(), style);

        if (brain.Navmeshagent == null) return;

        if (brain.Navmeshagent.pathStatus == NavMeshPathStatus.PathComplete)
        {
            var path = brain.Navmeshagent.path.corners;
            Gizmos.color = pathColor;
            Gizmos.DrawSphere(path[path.Length - 1], 0.2f);

            for (int i = 1; i < path.Length; i++)
            {
                var ofset = Vector3.up * pathOfset;
                Gizmos.DrawLine(path[i - 1] + ofset, path[i] + ofset);
            }
        }
    }
}