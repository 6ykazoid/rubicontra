﻿public class HeroBrain : Brain
{
    public override CharacterState DefaultState()
    {
        return new Idle(this);
    }

    public void TrySwichState(CharacterState newState)
    {
        base.SwitchState(newState);
    }
}
