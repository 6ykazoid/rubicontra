﻿using UnityEngine.AI;

public abstract class CharacterState
{
    protected Brain brain;
    protected NavMeshAgent navmeshAgent;

    public CharacterState NextState { get; set; }

    public CharacterState(Brain brain)
    {
        this.brain = brain;
        this.navmeshAgent = brain.Navmeshagent;
    }

    public abstract void Tick();
    public abstract void EnterState();
    public abstract void ExitState();
}