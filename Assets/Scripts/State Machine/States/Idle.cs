﻿using UnityEngine;

public class Idle : CharacterState
{
    public Idle(Brain brain) : base(brain) { }

    public override void EnterState()
    {
    }

    public override void ExitState()
    {
    }

    public override void Tick()
    {
    }
}