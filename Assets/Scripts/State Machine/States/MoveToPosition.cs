﻿using UnityEngine;

public class MoveToPosition: CharacterState
{
    private Vector3 destination;

    public MoveToPosition(Brain brain, Vector3 destination) : base(brain)
    {
        this.destination = destination;
    }

    public override void EnterState()
    {
        navmeshAgent.SetDestination(destination);
    }

    public override void ExitState()
    {
        
    }

    public override void Tick()
    {
        if(!navmeshAgent.pathPending && navmeshAgent.remainingDistance <= 0.05)
        {
            NextState = new Idle(brain);
        }
    }

    public override string ToString()
    {
        return string.Format("Moving to {0}", navmeshAgent.destination);
    }
}